#encoding: utf-8
from exts import db

# class user_roles(db.Model):
#     __tablename__ = 'user_roles'
#
#     id = db.Column(db.Integer, primary_key=True, autoincrement=True)
#     name = db.Column(db.String(128), unique=True)
#     role_type = db.Column(db.Integer)
#     # user = db.relationship('user', backref='user_type')
#     def __repr__(self):
#          return  'User_type: %d' % self.role_type


class user(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(128), unique=True)
    password = db.Column(db.String(128))
    # first_name = db.Column(db.String(128))
    # last_name = db.Column(db.String(128))
    # last_login = db.Column(db.DATETIME(6))
    # role_type = db.Column(db.Integer)
    # email = db.Column(db.String(128))
    # is_active = db.Column(db.Integer)
    # date_joined = db.Column(db.DATETIME(6))

    # def __repr__(self):
    #      return  'User: %s' % self.username