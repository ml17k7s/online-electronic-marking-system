# encoding: utf-8

from flask import Flask, render_template, request, redirect, url_for, session
import config
import pymysql
from models import user
from exts import db

pymysql.install_as_MySQLdb()
app = Flask(__name__)
app.config.from_object(config)
db.init_app(app)


@app.route('/')
def index():
    return render_template('index.html')
def index2():
    return render_template('index2.html')
def index3():
    return render_template('index3.html')



@app.route('/login/', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    else:
        username = request.form.get('username')
        password = request.form.get('password')
        users = user.query.filter(user.username == username, user.password == password).first()

        if users:
            session['id'] = users.id
            if session['id'] == 2:
                return render_template('index2.html')
            else:
                if session['id'] == 3:
                    return render_template('index3.html')
                else:
                    return redirect(url_for('index'))
        # return redirect(url_for('index'))
        else:
            return "error"


#
# @app.route('/regist/',methods=['GET','POST'])
# def regist():
#     if request.method == 'GET':
#         return render_template('regist.html')
#     else:
#         username = request.form.get('username')
#         email = request.form.get('emial')
#         password = request.form.get('password')
#         # re_password = request.form.get('re_password')
#
#         # users = user(username=username,email=email,password=password,re_password=re_password)
#         users = user(username=username, email=email, password=password)
#         db.session.add(users)
#         db.session.commit()
#         return redirect(url_for('login'))
#
# @app.route('/forgot/',methods=['GET','POST'])
# def forgot():
#     if request.method == 'GET':
#         return render_template('forgot.html')
#     else:
#         pass


@app.route('/exam/')
def exam():
    return render_template('exam.html')
@app.route('/exam2/')
def exam2():
    return render_template('exam2.html')
@app.route('/exam3/')
def exam3():
    return render_template('exam3.html')
@app.route('/examadd/')
def examadd():
    return render_template('examadd.html')
@app.route('/examadd3/')
def examadd3():
    return render_template('examadd3.html')
@app.route('/student/')
def student():
    return render_template('student.html')
@app.route('/student2/')
def student2():
    return render_template('student2.html')
@app.route('/student3/')
def student3():
    return render_template('student3.html')
@app.route('/studentadd/')
def studentadd():
    return render_template('studentadd.html')
@app.route('/teacher/')
def teacher():
    return render_template('teacher.html')
@app.route('/teacher3/')
def teacher3():
    return render_template('teacher3.html')
@app.route('/administrator/')
def administrator():
    return render_template('administrator.html')
@app.route('/module/')
def module():
    return render_template('module.html')
@app.route('/module2/')
def module2():
    return render_template('module2.html')
@app.route('/module3/')
def module3():
    return render_template('module3.html')
if __name__ == '__main__':
    app.run()
